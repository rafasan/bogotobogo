#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include "mydialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionNew_Window_triggered();
    void on_actionNonModalDialogue_triggered();

private:
    Ui::MainWindow *ui;
    MyDialog *mDialog_; //Pointer to a dialog
};

#endif // MAINWINDOW_H
