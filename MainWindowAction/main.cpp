#include "mainwindow.h"
#include <QApplication>

/*
 * Application launches a dialogue on the click of a menu item
 * URL: https://www.bogotobogo.com/Qt/Qt5_MainWindowTriggerAction.php
 *
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
