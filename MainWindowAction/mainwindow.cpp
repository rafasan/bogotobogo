#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mydialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_Window_triggered()
{
    qDebug() << "Click!";
    MyDialog mDialog;
    mDialog.setModal(true);
    mDialog.exec();
    mDialog.show();
}

void MainWindow::on_actionNonModalDialogue_triggered()
{
    // This dialogue would disappear as soon as created
    // mDialog.show();

    // Setting a property with a pointer instead keeps it alive
    mDialog_ = new MyDialog(this);
    mDialog_ -> show();
}
