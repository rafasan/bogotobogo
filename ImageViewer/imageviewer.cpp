#include "imageviewer.h"
#include "ui_imageviewer.h"

ImageViewer::ImageViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ImageViewer)
{
    ui->setupUi(this);

    // Create the label (store in property)
    imageLabel_ = new QLabel;
    imageLabel_ -> setBackgroundRole(QPalette::Base);
    imageLabel_ -> setSizePolicy(QSizePolicy::Ignored,
                                 QSizePolicy::Ignored);
    imageLabel_ -> setScaledContents(true);

    // Create scroll area (store in property)
    scrollArea_ = new QScrollArea;
    scrollArea_ -> setBackgroundRole(QPalette::Dark);
    scrollArea_ -> setWidget(imageLabel_);

    // Make the scrollArea the central widget
    setCentralWidget(scrollArea_);

    //Adjust the window
    setWindowTitle(tr("Image Viewer"));
    resize(500,400);

    // Reference an image
    QImage image("C:/Users/Rafael Sanchez/Documents/Code/QtTutorial/ImageViewer/cartadeajuste.png");

    // set the pixmap for the label to the test imag
    imageLabel_ ->setPixmap(QPixmap::fromImage(image));

    // create open menu item
    openAct_ = new QAction(tr("&Open...;"), this);
    openAct_ -> setShortcut(tr("Ctrl+O"));

    // create print menu item
    printAct_ = new QAction(tr("&Print...;"), this);
    printAct_ -> setShortcut(tr("Ctrl+P"));
    printAct_ -> setEnabled(false);

    // create exit menu item
    exitAct_ = new QAction(tr("E&xit;"), this);
    exitAct_ -> setShortcut(tr("Ctrl+Q"));

    // create zoom in menu item
    zoomInAct_ = new QAction(tr("Zoom &In; (25%)"), this);
    zoomInAct_ -> setShortcut(tr("Ctrl+=")); //ctrl +
    zoomInAct_ -> setEnabled(false);

    // create zoom out menu item
    zoomOutAct_ = new QAction(tr("Zoom &Out; (25%)"), this);
    zoomOutAct_ -> setShortcut(tr("Ctrl+-")); //ctrl +
    zoomOutAct_ -> setEnabled(false);

    // create normal size menu item
    normalSizeAct_ = new QAction(tr("&Normal; Size"), this);
    normalSizeAct_ -> setShortcut(tr("Ctrl+S")); //ctrl +
    normalSizeAct_ -> setEnabled(false);

    // create a fit to window menu item
    fitToWindowAct_ = new QAction(tr("&Fit; to Window"), this);
    fitToWindowAct_ -> setShortcut(tr("Ctrl+F"));
    fitToWindowAct_ -> setEnabled(false);
    fitToWindowAct_ -> setCheckable(true);

    // create help/about actions
    aboutAct_ = new QAction(tr("&About;"), this);
    aboutQtAct_ = new QAction(tr("About &Qt;"), this);

    // create file menu
    fileMenu_ = new QMenu(tr("&File"), this);
    fileMenu_ -> addAction(openAct_);
    fileMenu_ -> addAction(printAct_);
    fileMenu_ -> addSeparator();
    fileMenu_ -> addAction(exitAct_);

    // create view menu
    viewMenu_ = new QMenu(tr("&File"), this);
    viewMenu_ -> addAction(zoomInAct_);
    viewMenu_ -> addAction(zoomOutAct_);
    viewMenu_ -> addAction(normalSizeAct_);
    viewMenu_ -> addAction(fitToWindowAct_);

    // create help menu
    helpMenu_ = new QMenu(tr("&Help;"), this);
    helpMenu_ -> addAction(aboutAct_);
    helpMenu_ -> addAction(aboutQtAct_);

    // add those menus to menubar
    menuBar() -> addMenu(fileMenu_);
    menuBar() -> addMenu(viewMenu_);
    menuBar() -> addMenu(helpMenu_);
}

ImageViewer::~ImageViewer()
{
    delete ui;
}
