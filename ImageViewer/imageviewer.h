#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
#include <QLabel>
#include <QScrollArea>

namespace Ui {
class ImageViewer;
}

class ImageViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit ImageViewer(QWidget *parent = nullptr);
    ~ImageViewer();

private:
    Ui::ImageViewer *ui;
    QLabel *imageLabel_;
    QScrollArea *scrollArea_;
    QAction *openAct_;
    QAction *printAct_;
    QAction *exitAct_;
    QAction *zoomInAct_;
    QAction *zoomOutAct_;
    QAction *normalSizeAct_;
    QAction *fitToWindowAct_;
    QAction *aboutAct_;
    QAction *aboutQtAct_;
    QMenu *fileMenu_;
    QMenu *viewMenu_;
    QMenu *helpMenu_;

};

#endif // IMAGEVIEWER_H
